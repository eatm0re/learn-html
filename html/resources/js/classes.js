function paintMain() {
  paint("main", "red")
}

function paintCity() {
  paint("city", "green")
}

function paint(classToPaint, color) {
  var elems = document.getElementsByClassName(classToPaint);
  for (var i = 0; i < elems.length; i++) {
    elems[i].style.color = color;
  }
}
