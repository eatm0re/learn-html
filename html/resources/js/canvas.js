var canvas;
var context;

window.onload = function (ev) {
  canvas = document.getElementById("canvas");
  context = canvas.getContext("2d");
};

window.onkeypress = function (ev) {
  var key = ev.key.toLowerCase();
  if (key === "z") {
    drawLine()
  } else if (key === "x") {
    drawCircle()
  } else if (key === "a") {
    drawText()
  } else if (key === "s") {
    strokeText()
  } else if (key === "q") {
    drawLinearGradient()
  } else if (key === "c") {
    clearCanvas()
  }
};

function drawLine() {
  context.beginPath();
  context.moveTo(randomWidth(), randomHeight());
  context.lineTo(randomWidth(), randomHeight());
  context.closePath();
  context.stroke();
}

function drawCircle() {
  context.beginPath();
  context.arc(randomWidth(), randomHeight(), 100 * Math.random(), 0, 2 * Math.PI);
  context.closePath();
  context.stroke();
}

function drawText() {
  context.font = "30px Arial";
  context.fillText("Hello World", randomWidth(), randomHeight());
}

function strokeText() {
  context.font = "36px 'Comic Sans MS'";
  context.strokeText("It is stroked text!", randomWidth(), randomHeight());
}

function drawLinearGradient() {
  var x0 = randomWidth();
  var y0 = randomHeight();
  var x1 = randomWidth();
  var y1 = randomHeight();
  var gradient = context.createLinearGradient(x0, y0, x1, y1);
  gradient.addColorStop(0, randomColor());
  gradient.addColorStop(1, randomColor());
  while (Math.random() >= 0.5) {
    gradient.addColorStop(Math.random(), randomColor());
  }
  context.fillStyle = gradient;
  context.fillRect(x0, y0, x1 - x0, y1 - y0);
}

function clearCanvas() {
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function randomWidth() {
  return canvas.width * Math.random();
}

function randomHeight() {
  return canvas.height * Math.random();
}

function randomColor() {
  var degree = 360 * Math.random();
  return "hsl(" + degree + ", 100%, 50%)";
}
